import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        int N = input.nextInt();
        int K = input.nextInt();
        int m1=0, m2=0, m3;
         	
        Queue<Integer> queue = new PriorityQueue<>(N);
        for (int i = 0; i < N; i++) 
            queue.add(input.nextInt());
        
        int count = 0;
        while(!queue.isEmpty() && queue.peek()<K){
            m1 = queue.remove();
            if(!queue.isEmpty()){
                 m2 = queue.remove();
                 m3 = m1 + 2 * m2;
                 queue.add(m3);
            
            count++;
            }
        }
        if(!queue.isEmpty())            System.out.println(count);
        else
            System.out.println("-1");
        }
    }