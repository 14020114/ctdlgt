import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int [] arr = new int[n];
        for (int i = 0; i < n; i++)
            arr[i] = input.nextInt();
        
       for (int i = 0; i < n; i++)
           for (int j = i + 1; j < n; j++)
                if (arr[i] > arr[j]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
            }
        for (int i = 0; i < n; i++)
            System.out.print(arr[i]+" ");
       }
    }
