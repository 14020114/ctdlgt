import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

class TestClass {
   
    public static void main(String args[] ) {
    	Scanner input = new Scanner(System.in);	
    	int testcase = input.nextInt();
    	int num = 0;
    	
    	while (testcase-- >0){
    		int N = input.nextInt();
    		int first = input.nextInt();
    		Stack<Integer> stack = new Stack<>();
    	 	stack.push(first);
    		while (N-- > 0){
    			String argument = input.next();
    			if (argument.charAt(0) == 'P'){
    						num = input.nextInt();
    						stack.push(num);
    						}
    			else if (argument.charAt(0) == 'B'){ 
    						int temp1 = stack.pop();
    						int temp2 = stack.pop();
    						stack.push(temp2);
    						stack.push(temp1);
    						stack.push(temp2);
    						}
    			}
    		System.out.println("Player "+stack.pop());	
    		}
    	
    	
    	
    	
    }
}