import java.io.*;
import java.util.*;

public class Solution {

    public static int GCD(int a, int b){
         if (a == b) return a;
         else if (a > b) return GCD(a - b, b);
         else return GCD(a, b - a);
        }
    
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        int a, b;
        a = input.nextInt();
        b = input.nextInt();
        System.out.println(""+GCD(a,b));
    }
}