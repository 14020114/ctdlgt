import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

   public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int size = scan.nextInt();
        int[] a = new int[size];
        for (int i = 0; i < size; i++) {
            a[i] = scan.nextInt();
            
        }
        Arrays.sort(a);
        int median;
        if(a.length%2 == 0){
            median = a[a.length/2]+a[(a.length/2)+1];
        }
        else
            median = a[a.length/2];
        System.out.println(median);
    }
}