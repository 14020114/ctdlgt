import java.util.Scanner;
import java.util.Stack;
 
public class RemoveFriend {
	private static Scanner input=new Scanner(System.in);
	public static void main(String[] args) {
		
		int t=input.nextInt();
		while(t-- > 0) {
			int n=input.nextInt();
			int k=input.nextInt();
			Stack<Integer> priorityStack=new Stack<>();
			for(int i=0;i<n;i++) {
				int priority=input.nextInt();
				if(priorityStack.empty()) {
					priorityStack.push(priority);
					//continue;
				}
				else {
					if(k > 0) {
						try {
							while(priority>priorityStack.peek() && k > 0) {
								priorityStack.pop();
								k--;
							} }
						catch(java.util.EmptyStackException ese) { }
						priorityStack.push(priority);
					}
					else {
						priorityStack.push(priority);
					}
				}
			}
			for(int proir : priorityStack) {
				System.out.print(proir+" ");
			}
			System.out.println();
		}
	}
 
}