import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int [] ar = new int[n];
        int [] b  = new int[100];
        for (int i = 0; i < n; i++){
            ar[i] = input.nextInt();
            b[ar[i]]++;
        }
        for (int i = 0; i < 100; i++)
            System.out.print(b[i]+" ");
    }
}