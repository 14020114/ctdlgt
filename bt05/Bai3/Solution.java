import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Stack<Integer> forward = new Stack<>();
        Stack<Integer> reverse = new Stack<>();
        
        int fe = 0, size = 0;
        int N = input.nextInt();
        while (N-- > 0)
            {
            int argument = input.nextInt();
            if (argument == 1)
                {
                int data = input.nextInt();
                if (size == 0)
                    fe = data;
                forward.push(data);
                size++;              
                }
            else if (argument == 2)
                { 
                if (size != 0)    
                    {
                    while (!forward.isEmpty())
                       reverse.push(forward.pop());
                    reverse.pop();
                    size--;
                if (size == 0)
                    {
                    fe = reverse.peek();
                    while (!reverse.isEmpty())
                        forward.push(reverse.pop());
                    }
                    }
                }
            else if (argument == 3) 
                {
                if (size == 0)
                    System.out.println("");
                else
                    System.out.println(fe);
            }
        }
    }
}