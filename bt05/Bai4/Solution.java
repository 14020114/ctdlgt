import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main (String [] args){
	Scanner input = new Scanner(System.in);
	int N = input.nextInt();
	String str = "";
	int argument = 0;

	Stack<String> stack= new Stack<>();
	while (N-- > 0){
		argument = input.nextInt();
		switch(argument){
			case 1:
				stack.push(str);
				str = str + input.next();
				break;
			case 2:
				argument = input.nextInt();
				stack.push(str);
				str = str.substring(0, str.length() - argument);
				break;
			case 3:
				argument = input.nextInt();
				System.out.println(str.charAt(argument - 1));
				break;
			case 4:
				str = stack.pop();
				}
			}

	}
}