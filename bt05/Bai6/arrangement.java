
import java.util.Scanner;

public class arrangement {
       
    public static void main (String []args){
        
        Scanner input = new Scanner(System.in);
        System.out.print("Input number of element: ");
        int N = input.nextInt();
        int [] arr = new int[N];
        System.out.println("  Now input element: ");       
        
        for (int i = 0; i < N; i++)
            arr[i] = input.nextInt();
        
        for (int i = 0; i < N; i++){
            for (int j = i + 1; j < N; j++){
                if (arr[j] < arr[i]){
                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
        for (int i = 0; i < N; i++)
            System.out.print(arr[i]+"  ");
    }
    
}
