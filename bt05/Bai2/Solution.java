import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            String s = in.next();
        
        boolean mark = true; 
        Stack<Character> bracket = new Stack<>();
        int i = 0;
        while ((mark == true) && (i < s.length() )){
            if ((s.charAt(i) == '{') ||  (s.charAt(i) == '[') || (s.charAt(i) == '('))
                bracket.push(s.charAt(i));
            else if (s.charAt(i) == '}')
            {
                if (bracket.isEmpty() || bracket.pop() != '{' )
                {   
                    mark = false;
                    System.out.println("NO");
                    break;
                }
            }
            else if (s.charAt(i) == ']')
            {
                if (bracket.isEmpty() || bracket.pop() != '[' )
                {   
                    mark = false;
                    System.out.println("NO");
                    break;
                }
            }
            else if (s.charAt(i) == ')')
            {
                if (bracket.isEmpty() || bracket.pop() != '(' )
                {   
                    mark = false;
                    System.out.println("NO");
                    break;
                }
            }
            i++;
        }
        if (mark == true)
            System.out.println("YES");
         
    }
} 
}