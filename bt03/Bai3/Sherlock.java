import java.util.Scanner;
 
public class Sherlock{
    
    public static void main(String[] args){
    
        Scanner input = new Scanner(System.in);
        System.out.print("Number of test case: ");
        int T = input.nextInt();
        
        for(int i = 0; i < T ; i++){
            String answer = "NO";
            System.out.print("Test case "+i+"- input array element number: ");
            int N = input.nextInt();
            int[] a = new int[N];
            int total = 0;
            for(int j = 0; j < N ; j++){
                a[j] = input.nextInt();
                total = total + a[j];
            }
            if(N == 1){
                answer = "YES"; //always YES
                System.out.println(answer);
                continue;
            }
            if(N==2){           //always NO
                System.out.println(answer);
                continue;
            }
            int left = 0;
            int right = total;
            for(int j = 1; j < N-1 ; j++){
                left = left + a[j-1];
                right = total - a[j] - left;
                if(left == right){
                    answer = "YES";
                    break;
                }
            }
            System.out.println(answer);
        }
    }
}