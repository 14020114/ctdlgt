import java.util.Scanner;
import java.util.Arrays;
public class Pairs{

    public static void main (String [] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Number of Array N = ");
        int N = input.nextInt();
        
        int [] arr = new int[N];
        
        System.out.println();
        System.out.print("Input K: ");
        int K = input.nextInt();
        System.out.println("Input element: ");
        
        for (int i = 0; i < N; i++){
            arr[i] = input.nextInt();
            }
        Arrays.sort(arr);
        int count = 0;
        
        
        for (int i = 0; i < N; i++){
            for (int j = i + 1; j < N; j++){
                if (arr[i] + K == arr[j]){
                    count++;
                    break;
                }
                
            }
        }
       
        System.out.println("Count = "+count);
    }
}