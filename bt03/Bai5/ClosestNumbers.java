import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ClosestNumbers
{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        
        System.out.print("Input number of elements N: ");
        int N = input.nextInt();
        int [] A = new int[N];
                
        System.out.println();
        System.out.println("Input element: ");
        for (int i = 0; i < N; i++){
            A[i] = input.nextInt();
            }
        
        Arrays.sort(A);
        ArrayList A1 = new ArrayList();
        int closet = 9999999;
        for (int i = 0; i < N-1; i++){
            
            if (Math.abs(A[i] - A[i+1]) < closet){
                A1.clear();
                A1.add(A[i]);
                A1.add(A[i + 1]);
                closet = Math.abs(A[i] - A[i+1]);
            }
            else if (Math.abs(A[i] - A[i+1]) == closet){
                A1.add(A[i]);
                A1.add(A[i + 1]);
            }
            }
        System.out.println(A1);
    }
}